package io.FallenDev.main;

import io.FallenDev.utils.Metrics;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by FallenDev
 */

public class Main extends JavaPlugin {

    public void onEnable() {
            try {
                Metrics metrics = new Metrics(this);
                metrics.start();
            } catch (IOException e) {

            }
        }

    public static ArrayList<String> EffectsEnabled = new ArrayList<String>();


   public boolean onCommand(CommandSender sender, Command cmd , String label,String[] args) {

       if (!sender.hasPermission("effects.use")) {
       sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use this command!");
       return true;
       }

       if (cmd.getName().equalsIgnoreCase("effects")) {

           if (!(sender instanceof Player)) {
               sender.sendMessage(ChatColor.RED + "You must be a player to use this command!");
               return true;
       }

           if (sender instanceof Player) {
               Player player = (Player) sender;

               if (!EffectsEnabled.contains(player.getName())) {
                   player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 2));
                   player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 2));
                   sender.sendMessage(ChatColor.GREEN + "Added Jump boost 3 and speed boost 3");
                   EffectsEnabled.add(player.getName());
                   return true;
               }else{
                   player.removePotionEffect(PotionEffectType.JUMP);
                   player.removePotionEffect(PotionEffectType.SPEED);
                   sender.sendMessage(ChatColor.GREEN + "Removed all potion effects.");
                   EffectsEnabled.remove(player.getName());
                   return true;
               }
           }
       }
       return true;
   }
}
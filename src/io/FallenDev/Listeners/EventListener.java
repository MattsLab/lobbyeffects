package io.FallenDev.Listeners;

import io.FallenDev.main.Main;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * Made by FallenYouth
 */

public class EventListener implements Listener {
    private Main plugin;

    public EventListener(Main plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_AIR) {

            if (event.getItem().getType() == Material.REDSTONE_TORCH_OFF) {
                Player player = event.getPlayer();

                if (!player.hasPermission("LobbyEffects.use.torch"))
                    return;

                if (!plugin.EffectsEnabled.contains(player.getName())) {
                    player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 2));
                    player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 2));
                    player.sendMessage(ChatColor.GREEN + "Added Jump boost 3 and speed boost 3");
                    plugin.EffectsEnabled.add(player.getName());
                }else if (player.isSneaking()) {
                    player.removePotionEffect(PotionEffectType.JUMP);
                    player.removePotionEffect(PotionEffectType.SPEED);
                    player.sendMessage(ChatColor.GREEN + "Removed all potion effects.");
                    plugin.EffectsEnabled.remove(player.getName());
                }
            }
        }
    }
}
